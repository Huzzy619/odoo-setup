import xmlrpc.client

from Odoo.models import OdooModels
from typing import Union

class Odoo:
    def __init__(self):
        url = "https://ssaf.odoo.com"
        common = xmlrpc.client.ServerProxy("{}/xmlrpc/2/common".format(url))

        self.username = "hussein.ibrahim6196@gmail.com"
        self.db = "ssaf"
        self.password = "@Huzkid619"

        self.uid = common.authenticate(self.db, self.username, self.password, {})

        self.models = xmlrpc.client.ServerProxy("{}/xmlrpc/2/object".format(url))

    def create(self, model_name: OdooModels, data: dict):
        id = self.models.execute_kw(
            self.db, self.uid, self.password, model_name.value, "create", [data]
        )

        return id

    def read(self, model_name: OdooModels, id: Union[int, list[int]], fields: list):
        if isinstance(id, int):
            read_param = [id]    
        else:
            read_param = id

        data = self.models.execute_kw(
            self.db,
            self.uid,
            self.password,
            model_name.value,
            "read",
            [read_param],
            {"fields": fields},
        )
        return data

    def search_read(
        self,
        model_name: OdooModels,
        filters: dict,
        fields: list,
        limit: int = 10,
        offset: int = 0,
    ):
        filters = []
        for k, v in filters.items():
            filters.append((k, "=", v))

        user_data = self.models.execute_kw(
            self.db,
            self.uid,
            self.password,
            model_name.value,
            "search_read",
            [filters],
            {"fields": fields, "limit": limit},
        )
        return user_data

    def update(self, model_name: OdooModels, id: int, update_data: dict, fields: list =None):
        #?  The fields parameter helps to specify the possible response we want back from the server 
        status = self.models.execute_kw(
            self.db, self.uid, self.password, model_name.value, "write", [[id], update_data]
        )
        if status:
            updated_obj = self.read(model_name=model_name, id=id, fields=fields or ["id", "name"])
            return updated_obj
        return status

    def delete(self, model_name: OdooModels, id: int):
        status = self.models.execute_kw(
            self.db, self.uid, self.password, model_name.value, "unlink", [[id]]
        )
        return status
