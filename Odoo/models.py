from enum import Enum
from pydantic import BaseModel


class OdooModels(Enum):
    Contact = "res.partner"
    Lead = "crm.lead"
    HelpDesk = "helpdesk.ticket"
    User = "res.users"
    Company = "res.company"
    Group = "res.groups"

class TicketType(Enum):
    Issue = "issue"
    Question = "question"

class Contact(BaseModel):
    name: str
    email: str
    phone: str
    # country: str
    # Other fields   

class Lead(BaseModel):
    name: str
    contact_name: str
    email_from: str
    phone: str
    # Other fields   



class HelpDesk(BaseModel):
    name: str
    # email: str =  #! not allowed
    partner_id: int
    # phone: str = None #! not allowed 
    type: TicketType = None
    description: str
    # Other fields

