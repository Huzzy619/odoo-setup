from Odoo.config import Odoo
from Odoo.models import OdooModels

odoo = Odoo()

# #? Create
# new_id = odoo.create(model_name=OdooModels.HelpDesk, data={"name": "Latest HuzzyKrane21", "email": "newhuzzy13@go.com"})

# print(new_id)

# #? Read
# new_user = odoo.read(model_name=OdooModels.HelpDesk, id=11, fields=["name", "email"])

# print(new_user)


# #? Update
# update_data = {"name": "Updated Name1", "email": "updated_email@example.com2"}
# updated_obj = odoo.update(model_name=OdooModels.HelpDesk, id=11, update_data=update_data)
# print(updated_obj)



#? Delete
status = odoo.delete(model_name=OdooModels.HelpDesk, id=13)

print(status)