from typing import Union
from Odoo.config import Odoo
from Odoo.models import OdooModels

odoo = Odoo()

def create_partner(data:  dict):
    new_id = odoo.create(model_name=OdooModels.Contact, data = data)
    return new_id

def read_partner(id: Union[int, list[int]]):
    new_user = odoo.read(model_name=OdooModels.Contact, id=id, fields=["name", "email"])
    return new_user

def update_partner(id: int, update_data: dict, fields =None):
    updated_obj = odoo.update(model_name=OdooModels.Contact, id=id, update_data=update_data, fields=fields)

    return updated_obj

def delete_partner(id: int):
    status = odoo.delete(model_name=OdooModels.Contact, id=id)
    return status 

#====================Lead====================
def create_lead(data:  dict):
    new_id = odoo.create(model_name=OdooModels.Lead, data = data)
    return new_id

def read_lead(id: Union[int, list[int]]):
    new_user = odoo.read(model_name=OdooModels.Lead, id=id, fields=["name"])
    return new_user

def update_lead(id: int, update_data: dict, fields: list =None):
    updated_obj = odoo.update(model_name=OdooModels.Lead, id=id, update_data=update_data, fields=fields)

    return updated_obj

def delete_lead(id: int):
    status = odoo.delete(model_name=OdooModels.Lead, id=id)
    return status 

#+===================HelpDesk====================

def create_ticket(data:  dict):
    new_id = odoo.create(model_name=OdooModels.HelpDesk, data = data)
    return new_id

def delete_ticket(id: int):
    status = odoo.delete(model_name=OdooModels.HelpDesk, id=id)
    return status 

def read_ticket(id: Union[int, list[int]]):
    new_user = odoo.read(model_name=OdooModels.HelpDesk, id=id, fields=["name"])
    return new_user

def update_ticket(id: int, update_data: dict, fields: list =None):
    updated_obj = odoo.update(model_name=OdooModels.HelpDesk, id=id, update_data=update_data, fields=fields)

    return updated_obj