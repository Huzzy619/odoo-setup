data = {
    "shipments": [
        {
            "states": [
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-20T13:39:00Z",
                    "carrier": 0,
                    "status": "6 pieces delivered at IAH to ."
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-20T00:41:00Z",
                    "carrier": 0,
                    "status": "6 pieces arrived at IAH warehouse. Ready for customer pick-up."
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-19T22:21:00Z",
                    "carrier": 0,
                    "status": "6 pieces checked in at IAH off DL1557/19OCT23."
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-19T22:14:00Z",
                    "carrier": 0,
                    "status": "6 pieces arrived at IAH on DL1557/19OCT23."
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-19T22:14:00Z",
                    "carrier": 0,
                    "status": "6 pieces arrived at IAH on DL1557/19OCT23."
                },
                {
                    "location": "Minneapolis St Paul Intl (MSP), Minneapolis, United States",
                    "date": "2023-10-19T20:07:00Z",
                    "carrier": 0,
                    "status": "2 pieces scanned onboard DL1557/19OCT23 for IAH."
                },
                {
                    "location": "Minneapolis St Paul Intl (MSP), Minneapolis, United States",
                    "date": "2023-10-19T20:07:00Z",
                    "carrier": 0,
                    "status": "4 pieces scanned onboard DL1557/19OCT23 for IAH."
                },
                {
                    "location": "MSP to IAH",
                    "date": "2023-10-19T19:49:00Z",
                    "carrier": 0,
                    "status": "2 pieces departed from MSP on DL1557/19OCT23 to IAH."
                },
                {
                    "location": "MSP to IAH",
                    "date": "2023-10-19T19:49:00Z",
                    "carrier": 0,
                    "status": "4 pieces departed from MSP on DL1557/19OCT23 to IAH."
                },
                {
                    "location": "Minneapolis St Paul Intl (MSP), Minneapolis, United States",
                    "date": "2023-10-19T13:32:00Z",
                    "carrier": 0,
                    "status": "6 pieces at MSP assigned to DL1557/19OCT23 to IAH."
                },
                {
                    "location": "Minneapolis St Paul Intl (MSP), Minneapolis, United States",
                    "date": "2023-10-19T13:32:00Z",
                    "carrier": 0,
                    "status": "6 pieces weighing 480.0 lbs accepted at MSP."
                }
            ],
            "origin": "United States",
            "destination": "United States",
            "carriers": [
                "Delta Airlines Cargo"
            ],
            "externalTracking": [
                {
                    "slug": "delta-airlines-cargo",
                    "url": "https://www.deltacargo.com/Cargo/trackShipment?awbNumber=00679079744",
                    "method": "GET",
                    "title": "Delta Airlines Cargo"
                }
            ],
            "attributes": [
                {
                    "l": "tracking_id",
                    "val": "006-79079744"
                },
                {
                    "l": "origin",
                    "val": "United States",
                    "code": "US"
                },
                {
                    "l": "destination",
                    "val": "United States",
                    "code": "US"
                },
                {
                    "l": "from",
                    "val": "MSP, Minneapolis St Paul Intl, Minneapolis"
                },
                {
                    "l": "to",
                    "val": "IAH, George Bush Intcntl Houston, Houston"
                },
                {
                    "l": "weight",
                    "val": "480.0 lb"
                },
                {
                    "l": "pieces",
                    "val": "6"
                },
                {
                    "l": "days_transit",
                    "n": "Days in transit",
                    "val": "4"
                }
            ],
            "services": [
                {
                    "slug": "delta-airlines-cargo",
                    "name": "Delta Airlines Cargo"
                }
            ],
            "detected": [
                0
            ],
            "detectedCarrier": {
                "name": "Delta Airlines Cargo",
                "slug": "delta-airlines-cargo"
            },
            "carrier": 0,
            "checkedCountry": "United States",
            "checkedCountryCode": "US",
            "destinationCode": "US",
            "originCode": "US",
            "status": "transit",
            "trackingId": "006-79079744",
            "lastState": {
                "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                "date": "2023-10-20T13:39:00Z",
                "carrier": 0,
                "status": "6 pieces delivered at IAH to ."
            }
        }
    ],
    "done": True,
    "fromCache": True
}