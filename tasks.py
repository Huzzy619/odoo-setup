import re
from datetime import datetime
from enum import Enum
from pprint import pprint
from time import sleep
from typing import Optional
from uuid import UUID

import httpx
from pydantic import BaseModel, conint, conlist, constr

# from delta import data
from british import data

# import sms.service.admin_awb_service as admin_awb_service
# from sms.models.awb_models import AwbGnsUpdate, AwbStatus, AwbTimeline, AwbUpdate


class WeightType(str, Enum):
    KG = "kg"
    LB = "lb"
    L = "l"


class UnitOfMeasure(str, Enum):
    METRIC = "METRIC"
    IMPERIAL = "IMPERIAL"


class FlightSegment(BaseModel):
    source_airport_code: constr(min_length=1)
    destination_airport_code: constr(min_length=1)
    source_city_country: constr(min_length=1)
    destination_city_country: constr(min_length=1)
    departure_terminal: Optional[constr(min_length=1)]
    arrival_terminal: Optional[constr(min_length=1)]
    scheduled_departure_timestamp_utc: datetime
    scheduled_arrival_timestamp_utc: datetime
    flight_number: Optional[constr(min_length=1)]
    progress_percentage: conint(ge=0, le=100)


class AwbStatus(str, Enum):
    DEPARTED = "DEPARTED"
    BOOKED = "BOOKED"
    READY_FOR_PICKUP = "READY_FOR_PICKUP"
    ACCEPTED_BY_AIRLINE = "ACCEPTED_BY_AIRLINE"
    MANIFESTED = "MANIFESTED"
    RECEIVED_FROM_FLIGHT = "RECEIVED_FROM_FLIGHT"
    DELIVERED = "DELIVERED"
    FLIGHT_CANCELED = "FLIGHT_CANCELED"
    RE_BOOKED = "RE_BOOKED"
    UNKNOWN = "UNKNOWN"
    ARRIVED = "ARRIVED"
    REMOVED_FROM_FLIGHT = "REMOVED_FROM_FLIGHT"


class AwbTimeline(BaseModel):
    status: AwbStatus
    detail: constr(min_length=1)
    timestamp_utc: datetime
    location: Optional[constr(min_length=1)]
    carrier: Optional[constr(min_length=1)]


class AwbGnsUpdate(BaseModel):
    awb_number: str
    awb_timeline: Optional[conlist(item_type=AwbTimeline, min_items=0)]
    status: AwbStatus
    flight_segments: Optional[conlist(item_type=FlightSegment, min_items=0)]
    weight: Optional[float]
    number_of_pieces: Optional[conint(ge=0)]
    unit_of_measure: Optional[UnitOfMeasure]
    estimated_departure_timestamp_utc: Optional[datetime]
    estimated_arrival_timestamp_utc: Optional[datetime]
    departure_timestamp_utc: Optional[datetime]
    carrier: Optional[constr(min_length=1)]


PARCEL_APP_API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIxNDdlOWY2MC03MzczLTExZWUtYWQ5NC1iMzZlMTljMmE0Y2YiLCJzdWJJZCI6IjY1Mzk3NzFiMzY1OTJiMjI0YzMzNTE0MCIsImlhdCI6MTY5ODI2NDg1OX0.YQbxZp35N9ITHizlx3KSp20Uhb0kjZvS8DIw6YpV-Yc"


async def get_tracking_info(awb_number: str):
    json_data = {
        "shipments": [
            {"trackingId": awb_number, "destinationCountry": "randomCountry"}
        ],
        "language": "en",
        "apiKey": PARCEL_APP_API_KEY,
    }
    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                "https://parcelsapp.com/api/v3/shipments/tracking", json=json_data
            )
            response.raise_for_status()

            response_data = response.json()

            if response_data.get("shipments", None):
                normalize_awb_response(response_data)

    except httpx.HTTPError as exc:
        raise Exception(f"HTTP Exception for {exc.request.url} - {exc}")


def normalize_awb_response(data: dict):
    latest_timeline = []
    for shipment in data["shipments"]:
        for state in shipment["states"]:
            awb_timeline = AwbTimeline(
                location=state["location"],
                timestamp_utc=parse_to_datetime(state["date"]),
                carrier=shipment["carriers"][state["carrier"]],
                status=determine_status(state["status"]),
                detail=state["status"],
            )
            latest_timeline.append(awb_timeline.dict())

    weight, unit_of_measure = get_weight_and_unit_of_measure(
        weight_str=data["shipments"][0]["attributes"][-3]["val"]
    )

    return AwbGnsUpdate(
        awb_number=data["shipments"][0]["trackingId"],
        awb_timeline=latest_timeline,
        status=determine_status(data["shipments"][0]["lastState"]["status"]),
        flight_segments=[],
        estimated_departure_timestamp_utc=parse_to_datetime(
            data["shipments"][0]["lastState"]["date"]  # Confirm this key
        ),
        weight=weight,
        number_of_pieces=int(data["shipments"][0]["attributes"][-2]["val"]),
        unit_of_measure=unit_of_measure,
        estimated_arrival_timestamp_utc=get_arrival_time(),
        departure_timestamp_utc=get_departure_time(),
        carrier=latest_timeline[0]["carrier"],
    )


def determine_status(status_str: str):
    for status in AwbStatus:
        if status.value.lower().replace("_", " ") in status_str.lower():
            return status
        if "Received" in status_str:
            return AwbStatus.DELIVERED

    return AwbStatus.UNKNOWN


def parse_to_datetime(date_str, date_format="%Y-%m-%dT%H:%M:%SZ"):
    try:
        date_obj = datetime.strptime(date_str, date_format)
        return date_obj.utcnow()
    except ValueError:
        return None  # Return None if the date string couldn't be parsed


def get_departure_time():
    return datetime.now()


def get_arrival_time():
    return datetime.now()


def get_weight_and_unit_of_measure(weight_str: str):
    # Using regular expression to extract digits and units
    match = re.match(r"(\d+\.\d+)\s*(\w+)", weight_str)

    if not match:
        raise Exception("Invalid weight format")

    digit = float(match.group(1))
    weight_type = WeightType(match.group(2))

    if weight_type == WeightType.KG:
        unit = UnitOfMeasure.METRIC
    else:
        unit = UnitOfMeasure.IMPERIAL
    return digit, unit


# print(get_weight_and_unit_of_measure("good morning"))

pprint(normalize_awb_response(data=data).dict())
