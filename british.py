data = {
    "shipments": [
        {
            "states": [
                {
                    "location": "Murtala Muhammed (LOS), Lagos, Nigeria",
                    "date": "2023-10-19T07:31:00Z",
                    "carrier": 0,
                    "status": "Received on flight. Flight BA075"
                },
                {
                    "location": "Murtala Muhammed (LOS), Lagos, Nigeria",
                    "date": "2023-10-18T17:41:00Z",
                    "carrier": 0,
                    "status": "Arrived. Flight BA0075"
                },
                {
                    "location": "Murtala Muhammed (LOS), Lagos, Nigeria",
                    "date": "2023-10-18T17:39:00Z",
                    "carrier": 0,
                    "status": "Arrived. Flight BA0075"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-18T11:55:00Z",
                    "carrier": 0,
                    "status": "Departed. Flight BA075 LONDON LHR to LAGOS LOS"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-18T05:32:00Z",
                    "carrier": 0,
                    "status": "Manifested. Flight BA075 LONDON LHR to LAGOS LOS"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-18T05:21:00Z",
                    "carrier": 0,
                    "status": "Pre-manifested. Flight BA075 LONDON LHR to LAGOS LOS"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-17T07:12:00Z",
                    "carrier": 0,
                    "status": "Received on flight. Flight BA194"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-17T07:10:00Z",
                    "carrier": 0,
                    "status": "Received on flight. Flight BA194"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-17T05:56:00Z",
                    "carrier": 0,
                    "status": "Arrived. Flight BA0194"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-17T05:55:00Z",
                    "carrier": 0,
                    "status": "Arrived. Flight BA194"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-17T05:55:00Z",
                    "carrier": 0,
                    "status": "Arrived. Flight BA0194"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T21:35:00Z",
                    "carrier": 0,
                    "status": "Departed. Flight BA194 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T20:17:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA194 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T20:17:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA194 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T17:10:00Z",
                    "carrier": 0,
                    "status": "Received. 3 Package/s - 185 kg"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T16:51:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA194 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-16T16:51:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA075 LONDON LHR to LAGOS LOS"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T16:02:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA194 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-16T16:02:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA075 LONDON LHR to LAGOS LOS"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-16T16:00:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA075 LONDON LHR to LAGOS LOS"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-16T16:00:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA194 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "George Bush Intcntl Houston (IAH), Houston, United States",
                    "date": "2023-10-15T23:38:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA196 HOUSTON IAH to LONDON LHR"
                },
                {
                    "location": "Heathrow (LHR), London, United Kingdom",
                    "date": "2023-10-15T23:38:00Z",
                    "carrier": 0,
                    "status": "Booked. Flight BA075 LONDON LHR to LAGOS LOS"
                }
            ],
            "origin": "United States",
            "destination": "Nigeria",
            "carriers": [
                "British Airways Cargo"
            ],
            "externalTracking": [
                {
                    "slug": "british-airways-cargo",
                    "url": "https://www.iagcargo.com/iagcargo/portlet/en/html/601/main/search?frame=True&awb.cia=125&awb.cod=56530235",
                    "method": "GET",
                    "title": "British Airways Cargo"
                }
            ],
            "attributes": [
                {
                    "l": "tracking_id",
                    "val": "125-56530235"
                },
                {
                    "l": "origin",
                    "val": "United States",
                    "code": "US"
                },
                {
                    "l": "destination",
                    "val": "Nigeria",
                    "code": "NG"
                },
                {   
                    "l": "from",
                    "val": "IAH, George Bush Intcntl Houston, Houston"
                },
                {
                    "l": "to",
                    "val": "LOS, Murtala Muhammed, Lagos"
                },
                {
                    "l": "weight",
                    "val": "185.00 kg"
                },
                {
                    "l": "packages",
                    "n": "Number of Packages",
                    "val": "3"
                },
                {
                    "l": "days_transit",
                    "n": "Days in transit",
                    "val": "8"
                }
            ],
            "services": [
                {
                    "slug": "british-airways-cargo",
                    "name": "British Airways Cargo"
                }
            ],
            "detected": [
                0
            ],
            "detectedCarrier": {
                "name": "British Airways Cargo",
                "slug": "british-airways-cargo"
            },
            "carrier": 0,
            "checkedCountry": "United States",  
            "checkedCountryCode": "US",
            "destinationCode": "NG",
            "originCode": "US",
            "status": "transit",
            "trackingId": "125-56530235",
            "lastState": {
                "location": "Murtala Muhammed (LOS), Lagos, Nigeria",
                "date": "2023-10-19T07:31:00Z",
                "carrier": 0,
                "status": "Received on flight. Flight BA075"
            }
        }
    ],
    "done": True,
    "fromCache": True
}