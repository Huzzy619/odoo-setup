import torch
from transformers import BertTokenizer, BertForSequenceClassification
import numpy as np
 
def check_status(input_string, known_statuses):
    # Load the pre-trained BERT model and tokenizer
    model_name = "bert-base-uncased"
    tokenizer = BertTokenizer.from_pretrained(model_name)
    model = BertForSequenceClassification.from_pretrained(model_name)
 
    # Tokenize the input string
    inputs = tokenizer(input_string, return_tensors="pt", truncation=True, padding=True)
 
    # Perform inference with the model
    with torch.no_grad():
        logits = model(**inputs).logits
 
    # Calculate softmax probabilities
    probabilities = torch.softmax(logits, dim=1).numpy()
    predicted_status_idx = np.argmax(probabilities)
 
    if probabilities[0][predicted_status_idx] > 0.5:
        predicted_status = known_statuses[predicted_status_idx]
        return f"The string contains the status: {predicted_status}"
 
    return "The status is not found in the string."
 
# Example usage:
known_statuses = ["active", "inactive", "pending", "completed", "canceled"]
input_string = "The order is currently active and will be completed soon."
result = check_status(input_string, known_statuses)
print(result)