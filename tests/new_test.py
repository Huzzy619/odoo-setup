import unittest

from Odoo.models import Contact, HelpDesk, Lead, TicketType
from Odoo.partners import (create_lead, create_partner, create_ticket,
                           delete_lead, delete_partner, delete_ticket,
                           read_lead, read_partner, read_ticket, update_lead,
                           update_partner, update_ticket)


class TestPartnersFunctions(unittest.TestCase):
    data = data = {
            "name": "John Doe",
            "email": "johndoe@example.com",
            "phone": "123-456-7890",
            # "country": "USA"
        }
    def test_create_partner(self):
        
        data = Contact(**self.data)
        new_id = create_partner(data = data.model_dump())

        self.assertIsNotNone(new_id)
        self.assertTrue(isinstance(new_id, int))

    def test_read_partner(self):
        
        data = Contact(**self.data)
        new_id = create_partner(data = data.model_dump())

        partner_id = new_id
        new_user = read_partner(id=partner_id)

        print(new_user)
        self.assertIsNotNone(new_user)
        self.assertEqual(new_user[0]["name"], self.data["name"])

    def test_update_partner(self):
        
        data = Contact(**self.data)
        new_id = create_partner(data = data.model_dump())

        partner_id = new_id
        update_data = {
            "email": "updated1@example.com"
        }
        updated_obj = update_partner(id = partner_id, update_data=update_data, fields=["email"])

        self.assertIsNotNone(updated_obj)
        self.assertEqual(updated_obj[0]["email"], update_data["email"])


    def test_delete_partner(self):
        
        data = Contact(**self.data)
        new_id = create_partner(data = data.model_dump())
        partner_id = new_id
        status = delete_partner(partner_id)
        self.assertTrue(status)




class TestCRMFunctions(unittest.TestCase):
    data = {
    "name": "New Lead",
    "contact_name": "John Doe",
    "email_from": "johndoe@example.com",
    "phone": "123-456-7890",
} 
    def test_create_lead(self):
        
        data = Lead(**self.data)
        new_id = create_lead(data = data.model_dump())

        self.assertIsNotNone(new_id)
        self.assertTrue(isinstance(new_id, int))

    def test_read_lead(self):
        
        data = Lead(**self.data)
        new_id = create_lead(data = data.model_dump())

        lead_id = new_id
        new_user = read_lead(id=lead_id)

        self.assertIsNotNone(new_user)
        self.assertEqual(new_user[0]["name"], self.data["name"])

    def test_update_lead(self):
        
        data = Lead(**self.data)
        new_id = create_lead(data = data.model_dump())

        lead_id = new_id
        update_data = {
            "name": "new name"
        }
        updated_obj = update_lead(id = lead_id, update_data=update_data, fields=["id", "name"])

        self.assertIsNotNone(updated_obj)
        self.assertEqual(updated_obj[0]["name"], update_data["name"])
        self.assertEqual(updated_obj[0]["id"], new_id)


    def test_delete_lead(self):
        
        data = Lead(**self.data)
        new_id = create_lead(data = data.model_dump())
        lead_id = new_id
        status = delete_lead(lead_id)
        self.assertTrue(status)




class TestHelpDeskFunctions(unittest.TestCase):
    data = {"description": "Hello World X2", "name": "Me again", "partner_id": 8, "phone": "123-456-7890", "ticket_type": TicketType.Question}
    def test_create_ticket(self):
        
        data = HelpDesk(**self.data)
        
        new_id = create_ticket(data = data.model_dump(exclude_none=True))

        self.assertIsNotNone(new_id)
        self.assertTrue(isinstance(new_id, int))

    def test_read_ticket(self):
        
        data = HelpDesk(**self.data)
        new_id = create_ticket(data = data.model_dump(exclude_none=True))

        ticket_id = new_id
        new_user = read_ticket(id=ticket_id)

        self.assertIsNotNone(new_user)
        self.assertEqual(new_user[0]["name"], self.data["name"])

    def test_update_ticket(self):
        
        data = HelpDesk(**self.data)
        new_id = create_ticket(data = data.model_dump(exclude_none=True))

        ticket_id = new_id
        update_data = {
            "name": "new name"
        }
        updated_obj = update_ticket(id = ticket_id, update_data=update_data, fields=["id", "name"])

        self.assertIsNotNone(updated_obj)
        self.assertEqual(updated_obj[0]["name"], update_data["name"])
        self.assertEqual(updated_obj[0]["id"], new_id)


    def test_delete_ticket(self):
        
        data = HelpDesk(**self.data)
        new_id = create_ticket(data = data.model_dump(exclude_none=True))
        ticket_id = new_id
        status = delete_ticket(ticket_id)
        self.assertTrue(status)



if __name__ == '__main__':
    unittest.main()