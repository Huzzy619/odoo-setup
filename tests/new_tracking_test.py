import unittest
from unittest.mock import AsyncMock, patch
from dump import initiate_tracking, read_tracking_result
import asyncio


class TestTrackingFunctions(unittest.TestCase):

    def setUp(self):
        # Create a test event loop
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

    def tearDown(self):
        # Close the test event loop
        self.loop.close()

    # Mock HTTP requests and responses
    def mock_httpx_requests(self, expected_status, response_data):
        patch_post = patch("httpx.AsyncClient.post", new_callable=AsyncMock)
        patch_get = patch("httpx.AsyncClient.get", new_callable=AsyncMock)

        mock_post = patch_post.return_value
        mock_get = patch_get.return_value

        mock_post.status_code = expected_status
        mock_post.json.return_value = response_data
        mock_get.status_code = expected_status
        mock_get.json.return_value = response_data

        return patch("httpx.AsyncClient", new_callable=AsyncMock), patch_post, patch_get

    def test_initiate_tracking_valid_awb_number(self):
        expected_status = 200
        response_data = {"uuid": "random_uuid"}

        with self.mock_httpx_requests(expected_status, response_data):
            result = self.loop.run_until_complete(initiate_tracking("valid_awb_number"))

        self.assertEqual(result, response_data)

    def test_initiate_tracking_invalid_awb_number(self):
        expected_status = 400
        response_data = {"error": "Invalid input"}

        with self.mock_httpx_requests(expected_status, response_data):
            result = self.loop.run_until_complete(initiate_tracking("invalid_awb_number"))

        self.assertEqual(result, response_data)

    def test_read_tracking_result_valid_tracking_id(self):
        expected_status = 200
        response_data = {"status": "delivered"}

        with self.mock_httpx_requests(expected_status, response_data):
            result = self.loop.run_until_complete(read_tracking_result("valid_tracking_id"))

        self.assertEqual(result, response_data)

    def test_read_tracking_result_invalid_tracking_id(self):
        expected_status = 400
        response_data = {"error": "Invalid input"}

        with self.mock_httpx_requests(expected_status, response_data):
            result = self.loop.run_until_complete(read_tracking_result("invalid_tracking_id"))

        self.assertEqual(result, response_data)

if __name__ == "__main__":
    unittest.main()
