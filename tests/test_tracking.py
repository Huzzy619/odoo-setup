import pytest
from unittest.mock import AsyncMock, patch
from dump import initiate_tracking, read_tracking_result

# Define a list of test cases with input parameters and expected outcomes
test_cases_initiate_tracking = [
    ("valid_awb_number", 200, {"uuid": "random_uuid"}),
    ("invalid_awb_number", 400, {"error": "Invalid input"}),
]

test_cases_read_tracking_result = [
    ("valid_tracking_id", 200, {"status": "delivered"}),
    ("invalid_tracking_id", 400, {"error": "Invalid input"}),
]

# Mock HTTP requests and responses
@pytest.fixture
def mock_httpx_requests():
    async def mock_post(*args, **kwargs):
        response_mock = AsyncMock()
        response_mock.status_code = kwargs["expected_status"]
        response_mock.json.return_value = kwargs["response_data"]
        return response_mock

    async def mock_get(*args, **kwargs):
        response_mock = AsyncMock()
        response_mock.status_code = kwargs["expected_status"]
        response_mock.json.return_value = kwargs["response_data"]
        return response_mock

    with patch("httpx.AsyncClient") as mock_client:
        mock_client.return_value.post = mock_post
        mock_client.return_value.get = mock_get
        yield

# Test initiate_tracking function
@pytest.mark.parametrize("awb_number, expected_status, response_data", test_cases_initiate_tracking)
@pytest.mark.asyncio
async def test_initiate_tracking(awb_number, expected_status, response_data, mock_httpx_requests):
    result = await initiate_tracking(awb_number)
    assert result == response_data

# Test read_tracking_result function
@pytest.mark.parametrize("tracking_id, expected_status, response_data", test_cases_read_tracking_result)
@pytest.mark.asyncio
async def test_read_tracking_result(tracking_id, expected_status, response_data, mock_httpx_requests):
    result = await read_tracking_result(tracking_id)
    assert result == response_data
