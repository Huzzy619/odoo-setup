from datetime import datetime
from enum import Enum
from pprint import pprint

import httpx

from time import sleep

# import spacy
from pydantic import BaseModel, constr

from delta import data


class AwbStatus(str, Enum):
    DEPARTED = "DEPARTED"
    BOOKED = "BOOKED"
    READY_FOR_PICKUP = "READY_FOR_PICK-UP"
    ACCEPTED_BY_AIRLINE = "ACCEPTED_BY_AIRLINE"
    MANIFESTED = "MANIFESTED"
    RECEIVED_FROM_FLIGHT = "RECEIVED_FROM_FLIGHT"
    DELIVERED = "DELIVERED"
    FLIGHT_CANCELED = "FLIGHT_CANCELED"
    RE_BOOKED = "RE_BOOKED"
    UNKNOWN = "UNKNOWN"
    ARRIVED = "ARRIVED"
    REMOVED_FROM_FLIGHT = "REMOVED_FROM_FLIGHT"


class AwbTimeline(BaseModel):
    status: AwbStatus
    detail: constr(min_length=1)
    timestamp_utc: datetime
    carrier: str
    location: str


# for status in AwbStatus:
#     print(status.value.lower().replace("_", " "))


def normalize_delta_response(data: dict):
    final_response = []
    for shipment in data["shipments"]:
        for state in shipment["states"]:
            awb_timeline = AwbTimeline(
                location=state["location"],
                timestamp_utc=parse_to_datetime(state["date"]),
                carrier=shipment["carriers"][state["carrier"]],
                status=determine_status(state["status"]),
                detail=state["status"],
            )
            final_response.append(awb_timeline)

    return final_response


def determine_status(status_str: str):
    for status in AwbStatus:
        if status.value.lower().replace("_", " ") in status_str.lower():
            return status

    return AwbStatus.UNKNOWN


def parse_to_datetime(date_str, date_format="%Y-%m-%dT%H:%M:%SZ"):
    try:
        date_obj = datetime.strptime(date_str, date_format)
        return date_obj
    except ValueError:
        return None  # Return None if the date string couldn't be parsed


def unveil(responses):
    import json

    for response in responses:
        print({**json.loads(response.json())})


# def check_status(input_string, known_statuses):
#     nlp = spacy.load("en_core_web_sm")
#     doc = nlp(input_string)

#     for status in known_statuses:
#         if status in [token.text.lower() for token in doc]:
#             return f"The string contains the status: {status}"

#     return "The status is not found in the string."


# # Example usage:
# # known_statuses = ["active", "inactive", "pending", "completed", "canceled"]
# # input_string = "The order is pending approval and currently active."
# known_statuses = [member.value.lower().replace("_", " ") for member in AwbStatus]
# # input_string = "6 pieces arrived at IAH warehouse. Ready for customer pick-up."
# input_string = "Planned (MAN)"
# result = check_status(input_string, known_statuses)

# print(result)


#  !python -m spacy download en_core_web_sm


# from time import time

# print(determine_status(status_str="ready for pick-up Arrived. Flight BA0075"))

# unveil(normalize_delta_response(data=data))

PARCELAPP_API_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI3ZTBiMDQ0MC03MjUyLTExZWUtOTA4Zi1hNzE5YWQyNjUyYTkiLCJzdWJJZCI6IjY1Mzc5MmYwNzI0MWMzNzEwY2U2MTExOSIsImlhdCI6MTY5ODE0MDkxMn0.3othNz9lmIM0IQ2yMeVp0vYAIF_Xxy1Lc4uqz0SVwqI"


async def initiate_tracking(awb_number: str):
    json_data = {
        "shipments": [
            {"trackingId": awb_number, "destinationCountry": "randomCountry"}
        ],
        "language": "en",
        "apiKey": PARCELAPP_API_KEY,
    }
    response_data =["uuid"] #Initializing response_data
    count = 0
    try:
        async with httpx.AsyncClient() as client:
            while "uuid" in response_data and count < 10:

                response = await client.post(
                "https://parcelsapp.com/api/v3/shipments/tracking", json=json_data
            )
                response.raise_for_status()

                response_data = response.json()
                count += 1

                pprint(response_data)
                sleep(20)

            return response_data
        
    except httpx.HTTPError as exc:
        print(f"HTTP Exception for {exc.request.url} - {exc}")


async def read_tracking_result(tracking_id: str):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"https://parcelsapp.com/api/v3/shipments/tracking?uuid={tracking_id}&apiKey={PARCELAPP_API_KEY}"
            )
            response.raise_for_status()
    except httpx.HTTPError as exc:
        print(f"HTTP Exception for {exc.request.url} - {exc}")


import asyncio

asyncio.run(initiate_tracking(awb_number="125-56526481"))

# awb_number = "001-80691225"
# response = httpx.get(f"https://parcelsapp.com/en/tracking/{awb_number}")

# print(response.text)